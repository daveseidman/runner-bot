const sx = 260;
const sy = 220;
const sw = 36;
const sh = 72;

const threshold = 9700;
const pauseBetweenJumps = 250;

const canvas = document.querySelector('.runner-canvas');
const context = canvas.getContext('2d');

const pressEnter = new KeyboardEvent('keydown', {
  altKey: false,
  bubbles: true,
  cancelBubble: false,
  cancelable: true,
  charCode: 0,
  code: "Enter",
  composed: true,
  ctrlKey: false,
  currentTarget: null,
  defaultPrevented: true,
  detail: 0,
  eventPhase: 0,
  isComposing: false,
  isTrusted: true,
  key: "Enter",
  keyCode: 32,
  location: 0,
  metaKey: false,
  repeat: false,
  returnValue: false,
  shiftKey: false,
  type: "keydown",
  which: 32
});

let jumping = false;

const update = () => {
  if(!jumping) {
    const { data } = context.getImageData(sx, sy, sw, sh);
    let value = 0;
    for(let i = 0, { length } = data; i < length; i += 4) {
      value += data[i];
    }

    if(value > threshold) {
      console.log('cactus!');
      document.body.dispatchEvent(pressEnter);
      jumping = true;
      setTimeout(() => { jumping = false; }, pauseBetweenJumps);
    }
  }
  requestAnimationFrame(update);
}

update();
