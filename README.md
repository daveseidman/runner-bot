# Chrome Dino Runner Bot

Copy and past the script into chrome dev tools console and hit enter. It will play the runner game for you. Show off to your friends by getting the top score.

![runner bot](runnerbot.gif)
